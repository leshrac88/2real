function getRandom(MIN, MAX)
{
    return Math.floor(Math.random() * (MAX - MIN + 1)) + MIN;
}

function generateArray(arr)
{
    for (var n = 0; n < arr.length; n++)
    {
        obj = new Object;
        obj.id = n;
        arr[n] = obj;
    }

    arr.sort(compareId);  //sort array by id
}

function compareId(prevObj, nextObj)
{
    return prevObj.id - nextObj.id;
}

function linearSearch(arr)
{
    for (l=0; l < 100000000; l++)
    {

    }
    for (i=0; i<arr.length; i++)
    {
       /* console.log(i);
        console.log(arrLength);*/
        if (arr[i].id == ID_SEARCH)
        {
            return i;
        }
    }
    return false;
}

function binarySearch(arr)
{
    first = 0;
    last = arr.length - 1;

    while (true)
    {
        count = last - first;

        if (count > 2)
        {
            if (count % 2 != 0)
            {
                count++;
            }
            mid = parseInt((last - first) / 2) + first;
        }
        else if (count >= 0)
        {
            mid = first;
        }
        else
        {
            return false;
        }

        if (arr[mid] == ID_SEARCH)
        {
            while ((mid != 0) && (arr[mid - 1] == ID_SEARCH))
                mid--;
            return mid;
        }
        else if (arr[mid] > ID_SEARCH)
        {
            last = mid - 1;
        }
        else
        {
            first = mid + 1;
        }
    }
}

ID_SEARCH = 1400088;
NUM_ITERATION = 10;

var MIN = 1;
var MAX = 10000000;

var timeEndLinear = new Date();
var timeStartLinear = new Date();
var timeLinear = 0;
var totalLinear = 0;

var timeEndBinary = new Date();
var timeStartBinary = new Date();
var timeBinary = 0;
var totalBinary = 0;

for (var k = 0; k < NUM_ITERATION; k++)
{
    var arr = new Array();
    generateArray(arr);
    arrLength = getRandom(MIN, MAX);

    timeStartLinear = Date.now();
    linearSearch(arr);
    timeEndLinear = Date.now();
    timeLinear = timeEndLinear - timeStartLinear;
    console.log("линейный поиск " + timeLinear + " мс");
    totalLinear += timeLinear;

    timeStartBinary = Date.now();
    binarySearch(arr);
    timeEndBinary = Date.now();
    timeBinary = timeEndBinary - timeStartBinary;
    console.log("бинарный поиск " + timeBinary + " мс");
    totalBinary += timeBinary;
}
/*
timeL = totalLinear / NUM_ITERATION;
console.log("линейный поиск " + timeL/1000 + " секунд");

timeB = totalBinary / NUM_ITERATION;
console.log("двличный поиск " + timeB/1000 + " секунд");*/
